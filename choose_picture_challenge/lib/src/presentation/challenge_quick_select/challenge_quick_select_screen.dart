import 'package:choose_picture_challenge/src/configs/configs.dart';
import 'package:choose_picture_challenge/src/configs/constant/app_end_point.dart';
import 'package:choose_picture_challenge/src/resources/models/challenge/challenge_model.dart';
import 'package:flutter/material.dart';
import 'package:choose_picture_challenge/src/presentation/presentation.dart';

class ChallengeQuickSelectScreen extends StatefulWidget {
  @override
  ChallengeQuickSelectScreenState createState() =>
      ChallengeQuickSelectScreenState();
}

class ChallengeQuickSelectScreenState
    extends State<ChallengeQuickSelectScreen> {
  ChallengeQuickSelectViewModel _viewModel;


  @override
  Widget build(BuildContext context) {
    return BaseWidget<ChallengeQuickSelectViewModel>(
      viewModel: ChallengeQuickSelectViewModel(
        state: this
      ),
      onViewModelReady: (viewModel) {
        _viewModel = viewModel;
        _viewModel.init();
      },
      builder: (context, viewModel, child) {
        return Scaffold(
          body: SafeArea(
            child: StreamBuilder(
              stream: viewModel.screenSubject.stream,
              builder: (context, snapshot) {
                if(snapshot.hasData){
                  return snapshot.data;
                }else{
                  return Container();
                }
              },
            ),
          ),
        );
      },
    );
  }

  buildGameScreen() {
    return Container(
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AppImages.bgLearn), fit: BoxFit.fill)),
      child: Column(
        children: [
          Expanded(
            child: _buildChallengeHeader(),
            flex: 2,
          ),
          Expanded(
            child: _buildChallengerBody(),
            flex: 3,
          ),
        ],
      ),
    );
  }

  _buildChallengeHeader() {
    return Stack(
      children: [
        Container(
          width: MediaQuery.of(context).size.width,
          padding: const EdgeInsets.all(4),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              _buildPlayerWaitingInfo(
                url: _viewModel.room.player1.userInfo.avatar,
                name: _viewModel.room.player1.userInfo.fullname,
                color: Colors.transparent,
                min: true,
              ),
              _buildPlayerWaitingInfo(
                url: _viewModel.room.player2.userInfo.avatar,
                name: _viewModel.room.player2.userInfo.fullname,
                color: Colors.transparent,
                min: true,
              ),
            ],
          ),
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(AppImages.bgChallengeHeader),
                  fit: BoxFit.fill)),
        ),
        Align(
            alignment: Alignment(-0.65, -0.9),
            child: StreamBuilder(
                stream: _viewModel.scoreAndHeartControllerPlayer1.stream,
                builder: (context, snapshot) {
                  if(snapshot.connectionState == ConnectionState.waiting){
                    return Container();
                  }
                  return _buildHeartAndScore(
                      heart: snapshot.data["heart"],
                      score: snapshot.data["score"]);
                })),
        Align(
            alignment: Alignment(0.65, -0.9),
            child: StreamBuilder(
                stream: _viewModel.scoreAndHeartControllerPlayer2.stream,
                builder: (context, snapshot) {
                  if(snapshot.connectionState == ConnectionState.waiting){
                    return Container();
                  }
                  return _buildHeartAndScore(
                      heart: snapshot.data["heart"],
                      score: snapshot.data["score"]);
                })),
        Align(
          alignment: Alignment(0, -0.96),
          child: _buildPanelQuest(),
        )
      ],
    );
  }

  _buildHeartAndScore({int heart, int score}) {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Wrap(
            children: _buildHearts(heart),
          ),
          Text(score.toString())
        ],
      ),
    );
  }

  _buildHearts(int heart) {
    List<Widget> list = [];
    for (var i = 0; i < heart; i++) {
      list.add(Image.asset(
        AppImages.icTurn,
        fit: BoxFit.fill,
        width: 25,
        height: 25,
      ));
    }
    return list;
  }

  _buildPanelQuest() {
    return Container(
      alignment: Alignment.center,
      width: 80,
      height: 40,
      padding: const EdgeInsets.all(4),
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AppImages.bgPanelTime), fit: BoxFit.fill)),
      child: StreamBuilder(
          stream: _viewModel.questNumController.stream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Text(
                "Câu ${snapshot.data}",
                style:
                    AppStyles.DEFAULT_LARGE_BOLD.copyWith(color: Colors.white),
              );
            } else {
              return Container();
            }
          }),
    );
  }

  _buildChallengerBody() {
    return Container(
        padding: const EdgeInsets.only(left: 60, right: 60),
        child: StreamBuilder(
          stream: _viewModel.questController.stream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Column(
                children: [
                  Expanded(
                    child: Text(
                      snapshot.data["data"].word,
                      style: AppStyles.DEFAULT_LARGE_BOLD,
                    ),
                    flex: 1,
                  ),
                  Expanded(
                    flex: 5,
                    child: Container(
                      width: _calculateWidthQuestGridView(
                          snapshot.data["data"].other.length + 1),
                      child: GridView.count(
                        childAspectRatio: 120 / 70,
                        crossAxisCount: (snapshot.data["data"].other.length >= 4
                            ? (snapshot.data["data"].other.length + 1) ~/ 2
                            : 2),
                        crossAxisSpacing: 20,
                        mainAxisSpacing: 20,
                        children: _buildGameQuestion(
                            snapshot.data["data"], snapshot.data["index"]),
                      ),
                    ),
                  )
                ],
              );
            } else {
              return Container();
            }
          },
        ));
  }

  _calculateWidthQuestGridView(int count) {
    var widthSize = MediaQuery.of(context).size.width;
    if (count <= 4) {
      return widthSize / 3;
    } else if (count <= 6) {
      return widthSize / 2;
    }
    return widthSize / 1.4;
  }

  _buildGameQuestion(Quest quest, int index) {
    List<Quest> quests = [quest];
    List<Widget> list = [];
    for (var item in quest.other) {
      quests.add(item);
    }
    quests.shuffle();

    for (var item in quests) {
      String url = item.image.replaceAll("public/", "storage/");
      list.add(
        StreamBuilder(
          stream: _viewModel.showCorrectController.stream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return WidgetGameImageChoose(
                nextQuestion: snapshot.data["nextQuestion"],
                correctAnswerId: quest.wid,
                thisId: item.wid,
                showCorrectId: snapshot.data["data"],
                url: url,
                paramFunction: () async =>
                    await _viewModel.onChooseAnswer(item.wid, index),
              );
            } else {
              return Container();
            }
          },
        ),
      );
    }
    return list;
  }

  _buildPlayerWaitingInfo({String url, String name, Color color, bool min}) {
    return Container(
      color: color,
      child: Column(
        mainAxisSize: min ? MainAxisSize.min : MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _buildCircleAvatar(url),
          Text(name),
        ],
      ),
    );
  }

  _buildCircleAvatar(String url) {
    if (url != null) {
      if (!url.contains("https"))
        url = "${AppEndPoint.BASE_SOCKET_URL}:${AppEndPoint.PORT}$url";
    }
    return Container(
      padding: const EdgeInsets.all(2),
      width: 60,
      height: 60,
      child: CircleAvatar(
        backgroundImage: url == null
            ? AssetImage(
                AppImages.icAvatarFind,
              )
            : NetworkImage(
                url,
              ),
      ),
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AppImages.icBgAvatar), fit: BoxFit.fill),
          borderRadius: BorderRadius.all(Radius.circular(100))),
    );
  }
}
