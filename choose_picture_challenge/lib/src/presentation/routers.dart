import 'package:choose_picture_challenge/src/presentation/presentation.dart';
import 'package:choose_picture_challenge/src/utils/widget_animation_navigator.dart';
import 'package:flutter/material.dart';

class Routers {
  static const String game_break_ball = "/game_break_ball";
  static const String game_fast = "/game_fast";
  static const String game_find_fast = "/game_find_fast";
  static const String game_find_word = "/game_find_word";
  static const String game_ligature = "/game_ligature";
  static const String game_slow = "/game_slow";
  static const String pronunciation_test = "/pronunciation_test";
  static const String challenge_quick_select = "/challenge_quick_select";
  static const String challenge_find_picture = "/challenge_find_picture";
  static const String challenge_combine = "/challenge_combine";

  static Route<dynamic> generateRoute(RouteSettings settings) {
    Object arguments = settings.arguments;
    switch (settings.name) {
      case challenge_quick_select:
        return WidgetAnimationNavigator(newPage: ChallengeQuickSelectScreen());
      case challenge_find_picture:
        return WidgetAnimationNavigator(newPage: ChallengeFindPictureScreen());
      case challenge_combine:
        return WidgetAnimationNavigator(newPage: ChallengeCombineScreen());
    }
  }
}
