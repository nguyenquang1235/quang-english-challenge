
import 'package:choose_picture_challenge/src/configs/configs.dart';
import 'package:choose_picture_challenge/src/configs/constant/app_end_point.dart';
import 'package:choose_picture_challenge/src/presentation/base/base.dart';
import 'package:choose_picture_challenge/src/presentation/challenge_find_picture/challenge_find_picture.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ChallengeFindPictureScreen extends StatefulWidget {
  @override
  _ChallengeFindPictureScreenState createState() =>
      _ChallengeFindPictureScreenState();
}

class _ChallengeFindPictureScreenState
    extends State<ChallengeFindPictureScreen> {
  ChallengeFindPictureViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<ChallengeFindPictureViewModel>(
      onViewModelReady: (viewModel) {
        _viewModel = viewModel;
        _viewModel.init(context);
      },
      viewModel: ChallengeFindPictureViewModel(state: this),
      builder: (context, viewModel, child) {
        return Scaffold(
          body: SafeArea(
            child: StreamBuilder(
              stream: viewModel.screenSubject.stream,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return snapshot.data;
                } else {
                  return Container();
                }
              },
            ),
          ),
        );
      },
    );
  }

  buildGameScreen() {
    return Container(
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AppImages.bgLearn), fit: BoxFit.fill)),
      child: Column(
        children: [
          Expanded(
            child: _buildChallengeHeader(),
            flex: 2,
          ),
          Expanded(
            child: _buildChallengerBody(),
            flex: 5,
          ),
        ],
      ),
    );
  }

  _buildChallengeHeader() {
    return Stack(
      children: [
        Container(
          width: MediaQuery.of(context).size.width,
          padding: const EdgeInsets.all(4),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              _buildPlayerWaitingInfo(
                url: _viewModel.room.player1.userInfo.avatar,
                name: _viewModel.room.player1.userInfo.fullname,
                color: Colors.transparent,
                min: true,
              ),
              _buildPlayerWaitingInfo(
                url: _viewModel.room.player2.userInfo.avatar,
                name: _viewModel.room.player2.userInfo.fullname,
                color: Colors.transparent,
                min: true,
              ),
            ],
          ),
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(AppImages.bgChallengeHeader),
                  fit: BoxFit.fill)),
        ),
        Align(
            alignment: Alignment(-0.65, -0.9),
            child: StreamBuilder(
                stream: _viewModel.scoreControllerPlayer1.stream,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return Container();
                  }
                  return Text(snapshot.data.toString());
                })),
        Align(
          alignment: Alignment(0.65, -0.9),
          child: StreamBuilder(
            stream: _viewModel.scoreControllerPlayer2.stream,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Container();
              }
              return Text(snapshot.data.toString());
            },
          ),
        ),
      ],
    );
  }

  _buildChallengerBody() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 35),
      child: GridView.count(
        mainAxisSpacing: 8,
        crossAxisSpacing: 8,
        crossAxisCount: 6,
        childAspectRatio: 100 / 70,
        children: _buildGameQuests(),
      ),
    );
  }

  _buildPlayerWaitingInfo({String url, String name, Color color, bool min}) {
    return Container(
      color: color,
      child: Column(
        mainAxisSize: min ? MainAxisSize.min : MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _buildCircleAvatar(url),
          Text(name),
        ],
      ),
    );
  }

  _buildCircleAvatar(String url) {
    if (url != null) {
      if (!url.contains("https"))
        url = "${AppEndPoint.BASE_SOCKET_URL}:${AppEndPoint.PORT}$url";
    }
    return Container(
      padding: const EdgeInsets.all(2),
      width: 60,
      height: 60,
      child: CircleAvatar(
        backgroundImage: url == null
            ? AssetImage(
                AppImages.icAvatarFind,
              )
            : NetworkImage(
                url,
              ),
      ),
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AppImages.icBgAvatar), fit: BoxFit.fill),
          borderRadius: BorderRadius.all(Radius.circular(100))),
    );
  }

  _buildGameQuests() {
    _viewModel.buildGameListFirst();
    _viewModel.addCallBackFunctionInGameList();
    return _viewModel.list;
  }

}
