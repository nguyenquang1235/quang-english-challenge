import 'dart:async';

import 'package:choose_picture_challenge/src/configs/constant/app_end_point.dart';
import 'package:choose_picture_challenge/src/presentation/base/base.dart';
import 'package:choose_picture_challenge/src/presentation/widgets/widget_game_find_image.dart';
import 'package:choose_picture_challenge/src/resources/resources.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:rxdart/rxdart.dart';

import '../presentation.dart';
import 'package:choose_picture_challenge/src/utils/utils.dart';

class ChallengeFindPictureViewModel extends BaseViewModel with SocketListener {
  SocketService service;
  QuestApplication questApplication;
  Room room;
  final dynamic state;
  List<dynamic> arraysOfIndex;
  List<dynamic> board;
  String compareWord;
  WidgetGameFindImage obj1;
  WidgetGameFindImage obj2;
  int maxChoice = 2;
  Timer _timer;
  List<WidgetGameFindImage> list = [];

  final nextQuestion = BehaviorSubject<bool>();
  final screenSubject = BehaviorSubject<Widget>();
  final roomSubject = BehaviorSubject<Room>();
  final scoreControllerPlayer1 = BehaviorSubject<int>();
  final scoreControllerPlayer2 = BehaviorSubject<int>();

  ChallengeFindPictureViewModel({this.state});

  init(BuildContext context) async {
    service = await SocketService.instance();
    service.setListener(this);
    service.listenEvent(SocketService.socket_find_picture_listener);
    maxChoice = 2;
    nextQuestion.sink.add(false);
    onConnect();
  }

  @override
  void dispose() async {
    service.emitOutRoom();
    service.offEvent(SocketService.socket_find_picture_listener);
    await _deleteCacheDir();
    super.dispose();
  }

  Future<void> _deleteCacheDir() async {
    final cacheDir = await getTemporaryDirectory();
    if (cacheDir.existsSync()) {
      cacheDir.deleteSync(recursive: true);
    }
  }

  onConnect() async {
    if (await service.isConnected) {
      Future.delayed(Duration(seconds: 2), () => service.emitSameFindPlayer(2));
      screenSubject.sink.add(StreamBuilder(
        stream: roomSubject.stream,
        builder: (context, snapshot) {
          return ChallengeWaitingPlayerScreen(
            room: snapshot.data ?? null,
          );
        },
      ));
    }
  }

  void onSameFindPlayer(data) async {
    room = Room.fromJson(await data);
    roomSubject.sink.add(room);
    scoreControllerPlayer1.sink.add(0);
    scoreControllerPlayer2.sink.add(0);
  }

  void onSamePlayQuestion(data) async {
    questApplication = QuestApplication.fromJson(await data["data"]);
    arraysOfIndex = await data["board"];
    if (questApplication != null) {
      await Future.wait(questApplication.quests.map((e) {
        String url = e.image.replaceAll("public/", "storage/");
        return ImageCacheUtils.cacheImageNetwork(
            url: AppEndPoint.BASE_IMAGE + "/" + url, context: context);
      }).toList());
      Future.delayed(Duration(seconds: 2), () => service.emitSamePlayReady());
    }
  }

  void onSamePlayShowQuestion(data) async {
    board = await (data["board"]);
    screenSubject.sink.add(state.buildGameScreen());
  }

  void onSamePlayAnswer(data) async {
    print(await data["uid"]);
    print(await data["answer"]["msg"]);
    _onSamePlayerAnswerTrue(data);
    _onUpdateScorePlayer(data);
  }

  void onSameEmotion(data) {}

  void onSamePlayEndGame(data) {
    Future.delayed(
        const Duration(milliseconds: 500), () => Navigator.pop(context));
  }

  void onSameOutRoom(data) {}

  buildGameListFirst() {
    var listData = questApplication.quests;
    var listIndex = arraysOfIndex;

    for (int i = 0; i < listIndex.length; i++) {
      for (int j = 0; j < listData.length; j++) {
        if (listIndex[i] == "${j}a") {
          list.add(WidgetGameFindImage(
            question: listData[j],
            boardId: i,
            isText: false,
          ));
        } else if (listIndex[i] == "${j}b") {
          list.add(WidgetGameFindImage(
            question: listData[j],
            boardId: i,
            isText: true,
          ));
        }
      }
    }
  }

  addCallBackFunctionInGameList() {
    for (int i = 0; i < list.length; i++) {
      list[i].callBack = () {
        _onGameItemCallBack(list[i]);
      };
    }
    return list;
  }

  _onGameItemCallBack(WidgetGameFindImage item) {
    if (item.isFlip) {
      if (obj1 != null && obj2 != null) {
        _checkMaxChoice();
      }

      _onFlipUpImage(item);
    } else if (!item.isFlip) {
      _onFlipDownImage(item);
    }

    _compareImage();
  }

  _onFlipUpImage(WidgetGameFindImage obj) {
    if (obj1 == null && obj.isFlip) {
      obj1 = obj;
    } else if (obj2 == null && obj.isFlip) {
      obj2 = obj;
    }
    maxChoice--;
  }

  _onFlipDownImage(WidgetGameFindImage obj) {
    if (obj1 == obj) {
      obj1 = null;
    } else {
      obj2 = null;
    }
    maxChoice++;
  }

  _checkMaxChoice() {
    if (maxChoice <= 0) {
      obj1.onFlip();
      obj2.onFlip();
      maxChoice = 2;
      _timer.cancel();
      obj1 = null;
      obj2 = null;
    }
  }

  _compareImage() async {
    if (obj1 != null && obj2 != null) {
      if (obj1.isFlip && obj2.isFlip && obj1 != obj2) {
        if (obj1.question.word == obj2.question.word) {
          _answer(obj1.boardId, obj2.boardId);
          obj1 = null;
          obj2 = null;
        } else {
          _timer = new Timer(const Duration(milliseconds: 350), () {
            if (obj1 != null && obj2 != null) {
              obj1.onFlip();
              obj2.onFlip();
              obj1 = null;
              obj2 = null;
            }
          });
        }
      }
    }
  }

  _onUpdateScorePlayer(dynamic data) {
    if (data["uid"] == room.player1.userInfo.uid) {
      scoreControllerPlayer1.sink.add(data["score"]);
    } else if (data["uid"] == room.player2.userInfo.uid) {
      scoreControllerPlayer2.sink.add(data["score"]);
    }
  }

  _onSamePlayerAnswerTrue(dynamic data) {
    bool isPlayer1 = data["uid"] == room.player1.userInfo.uid;

    list[data["answer"]["index1"]].isPlayer1 = isPlayer1;
    list[data["answer"]["index2"]].isPlayer1 = isPlayer1;

    if (!list[data["answer"]["index1"]].isFlip) {
      list[data["answer"]["index1"]].onFlip();
    }
    if (!list[data["answer"]["index2"]].isFlip) {
      list[data["answer"]["index2"]].onFlip();
    }

    list[data["answer"]["index1"]].onHide();
    list[data["answer"]["index2"]].onHide();
  }

  _answer(int index1, int index2) {
    service.emitSamePlayAnswer(index1: index1, index2: index2);
  }

  _resetStream() {
    roomSubject.sink.add(null);
    scoreControllerPlayer1.sink.add(0);
    scoreControllerPlayer2.sink.add(0);
  }
}
