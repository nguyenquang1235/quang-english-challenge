import 'package:cached_network_image/cached_network_image.dart';
import 'package:choose_picture_challenge/src/configs/constant/constant.dart';
import 'package:choose_picture_challenge/src/presentation/base/base.dart';
import 'package:choose_picture_challenge/src/presentation/challenge_combine/challenge_combine.dart';
import 'package:choose_picture_challenge/src/resources/resources.dart';
import 'package:flutter/material.dart';

import '../../presentation.dart';

class FindWordWithImageScreen extends StatefulWidget {
  ChallengeCombineViewModel _viewModel;

  FindWordWithImageScreen({Key key, ChallengeCombineViewModel viewModel})
      : super(key: key) {
    _viewModel = viewModel;
  }

  @override
  _FindWordWithImageScreenState createState() =>
      _FindWordWithImageScreenState();
}

class _FindWordWithImageScreenState extends State<FindWordWithImageScreen> {
  @override
  Widget build(BuildContext context) {
    return BaseWidget<ChallengeCombineViewModel>(
      viewModel: widget._viewModel,
      builder: (context, viewModel, child) {
        return _buildGameFindWordWithImage();
      },
    );
  }

  Widget _buildGameFindWordWithImage() {
    Quest question = widget._viewModel.questApplication.quests[0];
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        _buildImageQuestion(widget._viewModel.convertImageLink(question.image)),
        Text("Chọn từ phù hợp với hình ảnh",
            style: AppStyles.DEFAULT_LARGE_BOLD),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: _buildListAnswer(question),
        )
      ],
    );
  }

  Widget _buildImageQuestion(String imageUrl) {
    return Container(
      width: 150,
      height: 120,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(AppImages.icCharacterDefault),
          fit: BoxFit.fill,
        ),
      ),
      child: Container(
        padding: EdgeInsets.only(bottom: 5, right: 2),
        child: Center(
          child: ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
            child: CachedNetworkImage(
              width: 130,
              height: 120,
              imageUrl: imageUrl,
              placeholder: (BuildContext context, String url) =>
                  Center(child: CircularProgressIndicator()),
              errorWidget: (context, url, error) => Image.asset(
                AppImages.icAvatar,
                height: 85,
                width: 85,
                fit: BoxFit.fill,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildAnswer(String text, int wid, Quest correct) {
    return StreamBuilder(
      stream: widget._viewModel.quest1Controller.stream,
      builder: (context, snapshot) {
        return WidgetGameCombineFindPics(
          paramFunction: () => widget._viewModel.service.emitCombinePlayAnswer(
            params: {
              "questionIndex": widget._viewModel.questIndex,
              "answer": wid,
            },
          ),
          text: text,
          thisId: wid,
          showCorrectId: snapshot.data,
          correctAnswerId: correct.wid,
        );
      },
    );
  }

  List<Widget> _buildListAnswer(Quest question) {
    List<Widget> list = [];
    list.add(_buildAnswer(question.word, question.wid, question));
    list.add(
        _buildAnswer(question.other[0].word, question.other[0].wid, question));
    list.shuffle();
    return list;
  }
}
