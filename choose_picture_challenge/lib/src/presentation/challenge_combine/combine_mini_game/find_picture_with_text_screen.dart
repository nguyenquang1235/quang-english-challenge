import 'package:choose_picture_challenge/src/configs/configs.dart';
import 'package:choose_picture_challenge/src/presentation/widgets/widget_game_find_image.dart';
import 'package:choose_picture_challenge/src/resources/models/challenge/challenge_model.dart';
import 'package:flutter/material.dart';

import '../../presentation.dart';

class FindPicturesWithTextScreen extends StatefulWidget {
  ChallengeCombineViewModel _viewModel;

  FindPicturesWithTextScreen({Key key, ChallengeCombineViewModel viewModel})
      : super(key: key) {
    _viewModel = viewModel;
  }

  @override
  _FindPicturesWithTextScreenState createState() =>
      _FindPicturesWithTextScreenState();
}

class _FindPicturesWithTextScreenState
    extends State<FindPicturesWithTextScreen> {
  @override
  Widget build(BuildContext context) {
    return _buildGameFindImageWithWord(widget._viewModel.questApplication.quests[1]);
  }

  Widget _buildGameFindImageWithWord(Quest question) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Text(question.mean, style: AppStyles.DEFAULT_LARGE_BOLD.copyWith(fontSize: 35)),
        Text("Chọn từ phù hợp với nghĩa", style: AppStyles.DEFAULT_LARGE_BOLD),
        Row(),
        // GestureDetector(
        //   onTap: () {},
        //   child: _buildImageQuestion(AppShared.GetLinkImage(question.other[0].image)),
        // ),
        // GestureDetector(
        //   onTap: () {},
        //   child: _buildImageQuestion(AppShared.GetLinkImage(question.other[1].image)),
        // ),
        // GestureDetector(
        //   onTap: () {},
        //   child: _buildImageQuestion(AppShared.GetLinkImage(question.other[2].image)),
        // ),
      ],
    );
  }

  _buildImageQuestion(String text, int wid, Quest correct){
    return StreamBuilder(
      stream: null,
      builder: (context, snapshot) {
        return WidgetGameImageChoose(
          nextQuestion: true,
          thisId: wid,
          correctAnswerId: correct.wid,
          showCorrectId: snapshot.data,
          // url: ,
          // paramFunction: ,
        );
      }
    );
  }

}
