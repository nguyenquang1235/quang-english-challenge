import 'package:cached_network_image/cached_network_image.dart';
import 'package:choose_picture_challenge/src/configs/constant/app_end_point.dart';
import 'package:choose_picture_challenge/src/configs/constant/constant.dart';
import 'package:choose_picture_challenge/src/presentation/base/base.dart';
import 'package:choose_picture_challenge/src/presentation/challenge_combine/challenge_combine.dart';
import 'package:choose_picture_challenge/src/resources/models/challenge/challenge_model.dart';
import 'package:flutter/material.dart';

import 'combine_mini_game/find_picture_with_text_screen.dart';

class WidgetCombineChallengeGame extends StatefulWidget {
  ChallengeCombineViewModel _viewModel;

  WidgetCombineChallengeGame({Key key, ChallengeCombineViewModel viewModel})
      : super(key: key) {
    _viewModel = viewModel;
  }

  @override
  _WidgetCombineChallengeGameState createState() =>
      _WidgetCombineChallengeGameState();
}

class _WidgetCombineChallengeGameState
    extends State<WidgetCombineChallengeGame> {
  @override
  Widget build(BuildContext context) {
    return BaseWidget<ChallengeCombineViewModel>(
      viewModel: widget._viewModel,
      builder: (context, viewModel, child) {
        return Container(
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(AppImages.bgLearn), fit: BoxFit.fill)),
          child: Column(
            children: [
              Expanded(
                child: _buildChallengeHeader(),
                flex: 2,
              ),
              Expanded(
                flex: 5,
                child: StreamBuilder(
                  stream: viewModel.questController,
                  builder: (context, snapshot) =>
                      _buildCombineGameQuestScreen(snapshot.data),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  _buildChallengeHeader() {
    return Stack(
      children: [
        Container(
          width: MediaQuery.of(context).size.width,
          padding: const EdgeInsets.all(4),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              _buildPlayerWaitingInfo(
                url: widget._viewModel.room.player1.userInfo.avatar,
                name: widget._viewModel.room.player1.userInfo.fullname,
                color: Colors.transparent,
                min: true,
              ),
              _buildPlayerWaitingInfo(
                url: widget._viewModel.room.player2.userInfo.avatar,
                name: widget._viewModel.room.player2.userInfo.fullname,
                color: Colors.transparent,
                min: true,
              ),
            ],
          ),
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(AppImages.bgChallengeHeader),
                  fit: BoxFit.fill)),
        ),
        Align(
            alignment: Alignment(-0.65, -0.9),
            child: StreamBuilder(
                stream: widget._viewModel.scoreControllerPlayer1.stream,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return Container();
                  }
                  return Text(snapshot.data.toString());
                })),
        Align(
          alignment: Alignment(0.65, -0.9),
          child: StreamBuilder(
            stream: widget._viewModel.scoreControllerPlayer2.stream,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Container();
              }
              return Text(snapshot.data.toString());
            },
          ),
        ),
      ],
    );
  }

  _buildPlayerWaitingInfo({String url, String name, Color color, bool min}) {
    return Container(
      color: color,
      child: Column(
        mainAxisSize: min ? MainAxisSize.min : MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _buildCircleAvatar(url),
          Text(name),
        ],
      ),
    );
  }

  _buildCircleAvatar(String url) {
    if (url != null) {
      if (!url.contains("https"))
        url = "${AppEndPoint.BASE_SOCKET_URL}:${AppEndPoint.PORT}$url";
    }
    return Container(
      padding: const EdgeInsets.all(2),
      width: 60,
      height: 60,
      child: CircleAvatar(
        backgroundImage: url == null
            ? AssetImage(
                AppImages.icAvatarFind,
              )
            : NetworkImage(
                url,
              ),
      ),
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AppImages.icBgAvatar), fit: BoxFit.fill),
          borderRadius: BorderRadius.all(Radius.circular(100))),
    );
  }

  Widget _buildCombineGameQuestScreen(int questIndex) {
    switch (questIndex) {
      case 0:
        return FindWordWithImageScreen(viewModel: widget._viewModel,);
      case 1:
        return FindPicturesWithTextScreen(viewModel: widget._viewModel,);
      case 2:
        return Center(
          child: Text("Quest 3"),
        );
      case 3:
        return Center(
          child: Text("Quest 4"),
        );
      case 4:
        return Center(
          child: Text("Quest 5"),
        );
      case 5:
        return Center(
          child: Text("Quest 6"),
        );
      case 6:
        return Center(
          child: Text("Quest 7"),
        );
      case 7:
        return Center(
          child: Text("Quest 8"),
        );
      default:
        return SizedBox();
    }
  }

}
