import 'package:cached_network_image/cached_network_image.dart';
import 'package:choose_picture_challenge/src/configs/constant/app_end_point.dart';
import 'package:flutter/material.dart';
import 'package:choose_picture_challenge/src/configs/constant/constant.dart';
import 'package:rxdart/rxdart.dart';

typedef void ParamFunction();


class WidgetGameImageChoose extends StatefulWidget {
    final String url;
    final ParamFunction paramFunction;
    final int correctAnswerId;
    final int thisId;
    final int showCorrectId;
    final bool nextQuestion;
  const WidgetGameImageChoose({Key key, this.url, this.paramFunction, this.correctAnswerId, this.thisId, this.showCorrectId, this.nextQuestion})
      : super(key: key);

  @override
  _WidgetGameImageChooseState createState() => _WidgetGameImageChooseState();
}

class _WidgetGameImageChooseState extends State<WidgetGameImageChoose> {
  final trueFalseController = BehaviorSubject<Widget>();

  @override
  Widget build(BuildContext context) {
    showCorrect();
    return Container(
      child: Stack(
        children: [
          GestureDetector(
            onTap: () => widget.paramFunction(),
            child: Container(
              width: 120,
              padding: const EdgeInsets.all(4),
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage(AppImages.icBgCardAbove),
                      fit: BoxFit.fill)),
              child: Image(
                image: CachedNetworkImageProvider(
                    AppEndPoint.BASE_IMAGE + "/" + widget.url,
                ),
              ),
            ),
          ),
          Opacity(
            opacity: 0.8,
            child: StreamBuilder(
              stream: trueFalseController.stream,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return snapshot.data;
                } else {
                  return Container();
                }
              },
            ),
          )
        ],
      ),
    );
  }

  _buildWidgetTrue() {
    return Container(
      width: 120,
      padding: const EdgeInsets.all(4),
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AppImages.icBgCardAboveGreen),
              fit: BoxFit.fill)),
      child: Center(
        child: Image.asset(
          AppImages.icTrue,
        ),
      ),
    );
  }

  _buildWidgetFalse() {
    return Container(
      width: 120,
      padding: const EdgeInsets.all(4),
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AppImages.icBgCardAboveRed), fit: BoxFit.fill)),
      child: Center(
        child: Image.asset(
          AppImages.icFalse,
        ),
      ),
    );
  }

  @override
  void dispose() {
    trueFalseController.close();
    super.dispose();
  }

  showCorrect(){
    if(widget.nextQuestion){
      trueFalseController.sink.add(Container());
    }
    if(widget.showCorrectId != null){
      if(widget.showCorrectId == widget.thisId){
        if(widget.correctAnswerId == widget.thisId){
          trueFalseController.sink.add(_buildWidgetTrue());
        }else{
          trueFalseController.sink.add(_buildWidgetFalse());
        }
      }
    }
  }


}
