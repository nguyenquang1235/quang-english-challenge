import 'package:choose_picture_challenge/src/configs/configs.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

import '../presentation.dart';

class WidgetGameCombineFindPics extends StatefulWidget {
  final String text;
  final ParamFunction paramFunction;
  final int correctAnswerId;
  final int thisId;
  final int showCorrectId;

  const WidgetGameCombineFindPics({Key key,
    this.text,
    this.paramFunction,
    this.correctAnswerId,
    this.thisId,
    this.showCorrectId,
  })
      : super(key: key);

  @override
  _WidgetGameCombineFindPicsState createState() =>
      _WidgetGameCombineFindPicsState();
}

class _WidgetGameCombineFindPicsState extends State<WidgetGameCombineFindPics> {
  final trueFalseController = BehaviorSubject<Widget>();

  @override
  Widget build(BuildContext context) {
    showCorrect();
    return Container(
      child: GestureDetector(
        onTap: () => widget.paramFunction(),
        child: StreamBuilder(
          stream: trueFalseController.stream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return snapshot.data;
            } else {
              return Container();
            }
          },
        ),
      ),
    );
  }

  _buildDefault(){
    return Container(
        width: 120,
        padding: const EdgeInsets.all(4),
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(AppImages.btnMenu),
                fit: BoxFit.fill)),
        child: Center(
          child: Text(
            widget.text,
            style: AppStyles.DEFAULT_LARGE_BOLD
                .copyWith(color: Colors.white),
          ),
        ));
  }

  _buildWidgetTrue() {
    return Container(
        width: 120,
        padding: const EdgeInsets.all(4),
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(AppImages.btnTrue), fit: BoxFit.fill)),
        child: Center(
          child: Text(
            widget.text,
            style: AppStyles.DEFAULT_LARGE_BOLD.copyWith(color: Colors.white),
          ),
        ));
  }

  _buildWidgetFalse() {
    return Container(
        width: 120,
        padding: const EdgeInsets.all(4),
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(AppImages.icBtnNextDisable),
                fit: BoxFit.fill)),
        child: Center(
          child: Text(
            widget.text,
            style: AppStyles.DEFAULT_LARGE_BOLD.copyWith(color: Colors.white),
          ),
        ));
  }

  @override
  void dispose() {
    trueFalseController.close();
    super.dispose();
  }

  showCorrect() {
    trueFalseController.sink.add(_buildDefault());
    if (widget.showCorrectId != null) {
      if (widget.showCorrectId == widget.thisId) {
        if (widget.correctAnswerId == widget.thisId) {
          trueFalseController.sink.add(_buildWidgetTrue());
        } else {
          trueFalseController.sink.add(_buildWidgetFalse());
        }
      }
    }
  }
}
