class AppValues {
  AppValues._();

  static const double HEIGHT_APP_BAR = 60;
  static const double HEIGHT_BOTTOM_BAR = 55;
}
