import 'package:choose_picture_challenge/src/resources/models/word.dart';

class Ligature {
  final String image;
  final String question;
  final List<Word> answers;

  const Ligature({this.image, this.question, this.answers});
}
