class Word {
  String vietnamese;
  String english;
  bool isSelected;
  bool isSuccess;
  bool isHidden;

  Word({
    this.vietnamese,
    this.english,
    this.isSelected = false,
    this.isSuccess = false,
    this.isHidden = false,
  });
}
