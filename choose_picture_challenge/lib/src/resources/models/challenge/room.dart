import '../../resources.dart';

class Room {
  Room({
    this.id,
    this.topicid,
    this.questionIndex,
    this.uidwin,
    this.questions,
    this.player1,
    this.player2,
  });

  String id;
  int topicid;
  int questionIndex;
  dynamic uidwin;
  dynamic questions;
  Player player1;
  Player player2;

  factory Room.fromJson(Map<String, dynamic> json) => Room(
    id: json["id"],
    topicid: json["topicid"],
    questionIndex: json["questionIndex"],
    uidwin: json["uidwin"],
    questions: json["questions"],
    player1: Player.fromJson(json["player1"]),
    player2: Player.fromJson(json["player2"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "topicid": topicid,
    "questionIndex": questionIndex,
    "uidwin": uidwin,
    "questions": questions,
    "player1": player1.toJson(),
    "player2": player2.toJson(),
  };
}
