import 'dart:convert';

import 'package:choose_picture_challenge/src/resources/models/challenge/challenge_model.dart';

class QuestApplication {
  final List<Quest> quests;

  QuestApplication({this.quests});

  factory QuestApplication.fromJson(dynamic data) {
    List<Quest> list = [];
    for(var item in data){
      list.add(Quest.fromJson(item));
    }
    return QuestApplication(quests: list);
  }

}
