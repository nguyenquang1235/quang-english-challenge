import '../../resources.dart';

class Player {
  Player({
    this.userInfo,
    this.state,
    this.heart,
    this.right,
    this.score,
    this.answers,
  });

  UserInfo userInfo;
  int state;
  int heart;
  int right;
  int score;
  List<int> answers;

  factory Player.fromJson(Map<String, dynamic> json) => Player(
    userInfo: UserInfo.fromJson(json["userInfo"]),
    state: json["state"],
    heart: json["heart"],
    right: json["right"],
    score: json["score"],
    answers: List<int>.from(json["answers"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "userInfo": userInfo.toJson(),
    "state": state,
    "heart": heart,
    "right": right,
    "score": score,
    "answers": List<dynamic>.from(answers.map((x) => x)),
  };
}