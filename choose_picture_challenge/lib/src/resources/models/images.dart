import 'package:choose_picture_challenge/src/configs/configs.dart';

class Images {
  String image;
  String imageHidden;
  String title;
  bool isHidden;
  bool isSelected;

  Images({
    this.image,
    this.imageHidden,
    this.title,
    this.isHidden = false,
    this.isSelected = false,
  });
}
