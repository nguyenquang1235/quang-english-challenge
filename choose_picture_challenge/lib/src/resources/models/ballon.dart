class Ballon {
  int xPos;
  int yPos;
  String title;
  String image;

  Ballon({this.xPos, this.yPos, this.title = "", this.image});
}
