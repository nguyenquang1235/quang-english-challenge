import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class ImageCacheUtils {
  static Future cacheImageNetwork({BuildContext context, String url}) =>
      precacheImage(CachedNetworkImageProvider(url), context);

  static Future cacheImageAsset({BuildContext context, String url}) =>
      precacheImage(AssetImage(url), context);

  static clear(){

  }
}
